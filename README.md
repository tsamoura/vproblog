# vProbLog
Implementation of the probabilistic reasoning algorithm from "Beyond the Grounding Bottleneck: Datalog Techniques for Inference in Probabilistic Logic Programs", AAAI 2020. This codebase builds upon [VLog](https://ojs.aaai.org/index.php/AAAI/article/view/9993). 


## Folder structure ##

This folder contains the following: 

* The sources to build vProbLog. 
* Linux shell scripts to reproduce the AAAI 2020 experiments (folder _scripts_). 

## Requirements ##

* CMake 2.8
* gcc 8.4 (and higher)
* g++ 8.4 (and higher)

## Building the engine 

We used CMake to ease the installation process. To build vProbLog, the following
commands should suffice:

```
mkdir build
cd build
cmake ..
make
```

External libraries should be automatically downloaded and installed in the same
directory. The only library that should be already installed is zlib, which is
necessary to read gzip files. This library is usually already present by
default.

If you want to build the DEBUG version of the program, including the web
interface: proceed as follows:

```
mkdir build_debug
cd build_debug
cmake -DCMAKE_BUILD_TYPE=Debug ..
make
```

The commands were tested on an Intel i7 processor running Ubuntu 18.04.5 LTS.

The successful execution of the instructions will produce the following:

* an executable called "vProbLog" for calling the engine via the command line;
* a shared library called "libvProbLog-core.so" for calling tgs within C++ applications.

### Reproducing the experiments from the AAAI 2020 paper ###

### Scripts ###

The Shell scripts to reproduce the experiments are inside the _scripts_ folder. 


## Running vProbLog

The arguments have as follows: 

```
./vProbLog mat \
 -e path-to-edb.conf \
--rules path-to-rules-file \
--prob_file path-to-mappings.csv \
--maxDepth D \
--storemat_path path-to-store-the-output-probabilistic-model \
--storemat_format csv \
--rewriteMultihead true \
--restrictedChase false \
--ignoreMagic true 
```
* **-e** apath to database configuration file. This argument is the same as in GLog. 
* **--rules** accepts the input rules file. In contrast to TGs, the rules should be transformed as described in _Beyond the Grounding Bottleneck: Datalog Techniques for Inference in Probabilistic Logic Programs_ in order for the engine to perform probabilistic reasoning. 
* **--prob_file** accepts a .csv file including one row for each fact in the database. Each row is of the from _fact\_id,fact\_probability_.  
* **--maxDepth** denotes the maximum reasoning depth. Default reasoning depth is unbound.  
* **--storemat_path** and **--storemat_format** are as in GLog, namely, they denote the folder to store the probabilistic model and the format of the stored files. Notice that the folder also includes a file named QX.txt including one row per answer along with its associated probability. 
* The arguments **-rewriteMultihead**, **--restrictedChase** and **--ignoreMagic** should be set to their prespecified values. 

Additionally, a _statistics.txt_ file is created which outputs the following statistics:

* max\_mem\_mb: pick memory consumption in MB.
* runtime\_ms: total time in ms to compute the probabilistic model.
* probability\_ms: total time in ms to compute the probability of the query answers given the leaves in the provevance of each answer.  

### Running the LUBM scenarios 

The following returns results for lubm_10/q01.
The output model includes a _q01.txt_ file storing the answers for q01 and the probability of each answer.  

```
./vProbLog mat \
 -e data/vProbLog/lubm_10/edb.conf \
--rules data/vProbLog/lubm_10/LUBM_L_Q1_magic.dlog \
--prob_file data/vProbLog/lubm_10/mappings.csv \
--storemat_path path-to-store-the-output-probabilistic-model \
--storemat_format csv \
--rewriteMultihead true \
--restrictedChase false \
--ignoreMagic true
```


### Running the DBpedia scenarios 

The following command returns results for Q2466.
The output model includes a _Q2466.txt_ file storing the answers for Q2466 and the probability of each answer.  

```
./vProbLog mat \
 -e data/vProbLog/dbpedia/edb-Q2466.conf \
--rules data/vProbLog/dbpedia/rules-Q2466.dlog \
--prob_file data/vProbLog/dbpedia/mappings.csv \
--storemat_path path-to-store-the-output-probabilistic-model \
--storemat_format csv \
--rewriteMultihead true \
--restrictedChase false \
--ignoreMagic true 
```

### Running the Claros scenarios 

The following command returns results for Q11.
The output model includes a _Q11.txt_ file storing the answers for Q11 and the probability of each answer.  

```
./vProbLog mat \
 -e data/vProbLog/claros/edb-Q11.conf \
--rules data/vProbLog/claros/rules-Q11.dlog \
--prob_file data/vProbLog/claros/mappings.csv \
--storemat_path path-to-store-the-output-probabilistic-model \
--storemat_format csv \
--rewriteMultihead true \
--restrictedChase false \
--ignoreMagic true
 ```

### Running the YAGO scenarios 

The following command eturns results for yago5/yago5\_1\Q15.
The output model includes a _Q15.txt_ file storing the answers for Q15 and the probability of each answer.  

```
./vProbLog mat \
 -e data/vProbLog/yago/yago5/yago5_1/edb-Q15.conf \
--rules data/vProbLog/yago/yago5/yago5_1/rules-Q15.dlog \
--prob_file data/vProbLog/yago/yago5/yago5_1/mappings.csv \
--storemat_path path-to-store-the-output-probabilistic-model \
--storemat_format csv \
--rewriteMultihead true \
--restrictedChase false \
--ignoreMagic true 
 ```

### Running the WN18RR scenarios 

The following command returns results for wn18rr10/wn18rr10\_1\Q219.
The output model includes a _Q219.txt_ file storing the answers for Q219 and the probability of each answer.  

```
./vProbLog mat \
 -e data/vProbLog/wn18rr/wn18rr10/wn18rr10_1/edb-Q219.conf \
--rules data/vProbLog/wn18rr/wn18rr10/wn18rr10_1/rules-Q219.dlog \
--prob_file data/vProbLog/wn18rr/wn18rr10/wn18rr10_1/mappings.csv \
--storemat_path path-to-store-the-output-probabilistic-model \
--storemat_format csv \
--rewriteMultihead true \
--restrictedChase false \
--ignoreMagic true 
 ```

### Running the smokers scenarios 

The following command returns results for the query smokers-10-20-1.
The output model includes two files, namely _q1.txt_ and _q2.txt_ storing the answers for q01 (all smokers in the database) and 
q02 (smokers with asthma) and the probability of each answer.  

```
./vProbLog mat \
--maxDepth 4 \
 -e data/vProbLog/smokers/smokers-10-20-1/edb.conf \
--rules data/vProbLog/smokers/smokers-10-20-1/rules \
--prob_file data/vProbLog/smokers/smokers-10-20-1/mappings.csv \
--storemat_path path-to-store-the-output-probabilistic-model \
--storemat_format csv \
--rewriteMultihead true \
--restrictedChase false \
--ignoreMagic true 
 ```
In the smokers scenarios it is crucial to specify the maximum reasoning depth as probabilistic reasoning is very slow, otherwise.

## Contributors

Efi **Tsamoura**, Samsung AI Research.


